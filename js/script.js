const texts = Array.from(document.querySelector('.our-services-content').children),
    tabs = Array.from(document.querySelector('.our-services-navigation').children),
    navigation = document.querySelector('.our-services-navigation');
let currentIndex = null;


hideText();

texts[0].style.display = "flex";

navigation.addEventListener('click', (e) => {
    hideText();
    removeActive();
    currentIndex = tabs.indexOf(e.target);
    texts[currentIndex].style.display = "flex";
    tabs[currentIndex].classList.add('our-services-navigation-item-active');

});


function hideText() {
    for (let text of texts) {
        text.style.display = "none";
    }
}

function removeActive() {
    for (let nav of tabs) {
        nav.classList.remove('our-services-navigation-item-active');
    }
}


const _CATEGORIES = Object.freeze([
    'Graphic Design',
    'Web Design',
    'Landing Pages',
    'Wordpress'
]);

const _IMAGECARDS = Object.freeze([
    {
        imgSrc: "./img/ImageCards/image13.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image14.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image15.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image16.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image17.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image18.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image19.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image20.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image21.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image22.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image23.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image24.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image1.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image2.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image3.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image4.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image5.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image6.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image7.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image8.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image9.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image10.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image11.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image12.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image13.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image14.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image15.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image16.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image17.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image18.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image19.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image20.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image21.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image22.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image23.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: "./img/ImageCards/image24.jpg",
        title: "CREATIVE DESIGN",
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }


]);

const galleryWrapper = document.querySelector('.gallery-wrapper');
//Creating Load More button
const loadBtn = document.createElement('button');
const loadBtnText = document.createElement('div');
const plusImg = document.createElement('img');

//Adding Load More button properties
plusImg.src = './img/plus-icon.png';
loadBtn.href = '#';
loadBtnText.innerHTML = 'Load More';
loadBtn.append(plusImg, loadBtnText);
loadBtn.classList.add('load-button');

//Creating gallery navigation and adding categories to it
const galleryNavigation = document.querySelector('.our-work-navigation'),
    galleryNavigationItems = Array.from(document.querySelectorAll('.our-work-item')),
    gallery = document.querySelector('.our-work-gallery');

let currentGalleryIndex = null,
    currentCategory = null,
    displayedImageCount = 1,
    imageIndex = 0;


for (let i = 1; i < galleryNavigationItems.length; i++) {
    galleryNavigationItems[i].dataset.category = _CATEGORIES[i - 1];
}

galleryNavigation.addEventListener('click', (event) => {

    if (currentGalleryIndex) {
        galleryNavigationItems[currentGalleryIndex].classList.remove('our-work-item-active');
    } else {
        galleryNavigationItems[0].classList.remove('our-work-item-active')
    }

    currentGalleryIndex = galleryNavigationItems.indexOf(event.target);
    galleryNavigationItems[currentGalleryIndex].classList.add('our-work-item-active');


    let type = event.target.dataset.category;
    console.log(type);
    let imagesToRender = filterImages(_IMAGECARDS, type);

    if (imagesToRender.length <= 12) {
        loadBtn.style.display = 'none';
    } else {
        loadBtn.style.display = 'flex';
    }

    gallery.innerHTML = '';
    console.log(gallery);
    displayedImageCount = 1;
    imageIndex = 0;
    displayGallery(imagesToRender, currentCategory);
});

loadBtn.addEventListener('click', () => {

    const loader = document.createElement('div');
    loader.classList.add('loader');

    loadBtn.replaceWith(loader);

    setTimeout(() => {
        loader.replaceWith(loadBtn);
        displayGallery(filterImages(_IMAGECARDS, currentCategory));
        if (displayedImageCount >= 36) {
            loadBtn.remove();
        }
    }, 2000);

});


displayGallery(filterImages(_IMAGECARDS));


function filterImages(images, type) {
    let filteredImages = [];

    if (type) {
        currentCategory = type;
        images.forEach(image => {
            const category = image.category;

            if (type === category) {
                filteredImages.push(image);
            }
        });

    } else {
        currentCategory = type;
        return images;
    }

    return filteredImages;

}


function displayGallery(imagesToRender) {


    let displayedImageCountOnSingleClick = 1;


    for (imageIndex; imageIndex < imagesToRender.length; imageIndex++) {

        if (displayedImageCount <= 36 && displayedImageCountOnSingleClick <= 12) {
            //Create card item and all its nested elements
            const {imgSrc, title, category} = imagesToRender[imageIndex],
                cardItem = document.createElement('div'),
                cardHover = document.createElement('div'),
                cardImage = document.createElement('img'),
                cardTitle = document.createElement('p'),
                cardCategory = document.createElement('p'),
                hoverImageContainer = document.createElement('a'),
                hoverLinkContainer = document.createElement('a'),
                hoverSearchContainer = document.createElement('div'),
                linkImg = document.createElement('div'),
                searchImg = document.createElement('div');
            console.log(imageIndex);
            // Add Attributes
            cardImage.src = imgSrc;
            cardImage.dataset.category = category;
            cardTitle.innerHTML = title;
            cardCategory.innerHTML = category;
            hoverLinkContainer.href = '#';
            hoverImageContainer.href = '#';


            //Add classes
            cardItem.classList.add('card-item');
            cardImage.classList.add('card-image');
            cardHover.classList.add('card-hover');
            cardTitle.classList.add('card-title');
            cardCategory.classList.add('card-category');
            hoverLinkContainer.classList.add('hover-link-container');
            hoverSearchContainer.classList.add('hover-search-container');
            linkImg.classList.add('hover-img', 'hover-img-link');
            searchImg.classList.add('hover-img', 'hover-img-search');


            //Add link and search icons to their containers
            hoverLinkContainer.append(linkImg);
            hoverSearchContainer.append(searchImg);

            //Wrap link and search icon containers to one container
            hoverImageContainer.append(
                hoverLinkContainer,
                hoverSearchContainer
            );

            //Gather all card hover elements
            cardHover.append(
                hoverImageContainer,
                cardTitle,
                cardCategory
            );

            //Add image
            cardItem.append(
                cardImage
            );


            cardItem.addEventListener('mouseenter', () => {
                cardItem.append(cardHover);

                cardItem.addEventListener('mouseleave', () => {
                    cardHover.remove();
                });
            });

            gallery.append(cardItem);
            displayedImageCountOnSingleClick++;
            displayedImageCount++;
        } else {
            break;
        }
    }


    galleryWrapper.append(gallery, loadBtn);
}
